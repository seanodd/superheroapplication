﻿namespace Superhero.Api.Models
{
    public class OldSuperheroSuperpower
    {
        public int SuperpowerId { get; set; }
        public int SuperheroId { get; set; }
    }
}
