﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Superhero.Api.Models;

namespace Superhero.Api.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class Superheroes1Controller : ControllerBase
    {
        private readonly SuperheroContext _context;

        public Superheroes1Controller(SuperheroContext context)
        {
            _context = context;
        }

        // GET: api/Superheroes1
        [HttpGet]
        public async Task<ActionResult<IEnumerable<Models.Superhero>>> GetSuperheros()
        {
          if (_context.Superheros == null)
          {
              return NotFound();
          }
            return await _context.Superheros.ToListAsync();
        }

        // GET: api/Superheroes1/5
        [HttpGet("{id}")]
        public async Task<ActionResult<Models.Superhero>> GetSuperhero(int id)
        {
          if (_context.Superheros == null)
          {
              return NotFound();
          }
            var superhero = await _context.Superheros.FindAsync(id);

            if (superhero == null)
            {
                return NotFound();
            }

            return superhero;
        }

        // PUT: api/Superheroes1/5
        // To protect from overposting attacks, see https://go.microsoft.com/fwlink/?linkid=2123754
        [HttpPut("{id}")]
        public async Task<IActionResult> PutSuperhero(int id, Models.Superhero superhero)
        {
            if (id != superhero.SuperheroId)
            {
                return BadRequest();
            }

            _context.Entry(superhero).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!SuperheroExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return NoContent();
        }

        // POST: api/Superheroes1
        // To protect from overposting attacks, see https://go.microsoft.com/fwlink/?linkid=2123754
        [HttpPost]
        public async Task<ActionResult<Models.Superhero>> PostSuperhero(Models.Superhero superhero)
        {
          if (_context.Superheros == null)
          {
              return Problem("Entity set 'SuperheroContext.Superheros'  is null.");
          }
            _context.Superheros.Add(superhero);
            await _context.SaveChangesAsync();

            return CreatedAtAction("GetSuperhero", new { id = superhero.SuperheroId }, superhero);
        }

        // DELETE: api/Superheroes1/5
        [HttpDelete("{id}")]
        public async Task<IActionResult> DeleteSuperhero(int id)
        {
            if (_context.Superheros == null)
            {
                return NotFound();
            }
            var superhero = await _context.Superheros.FindAsync(id);
            if (superhero == null)
            {
                return NotFound();
            }

            _context.Superheros.Remove(superhero);
            await _context.SaveChangesAsync();

            return NoContent();
        }

        private bool SuperheroExists(int id)
        {
            return (_context.Superheros?.Any(e => e.SuperheroId == id)).GetValueOrDefault();
        }
    }
}
