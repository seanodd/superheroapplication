﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace Superhero.Api.Migrations
{
    public partial class Initial : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "Superheros",
                columns: table => new
                {
                    SuperheroId = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Name = table.Column<string>(type: "nvarchar(max)", nullable: false),
                    Nickname = table.Column<string>(type: "nvarchar(max)", nullable: false),
                    PhoneNumber = table.Column<string>(type: "nvarchar(max)", nullable: false),
                    DateOfBirth = table.Column<DateTime>(type: "datetime2", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Superheros", x => x.SuperheroId);
                });

            migrationBuilder.CreateTable(
                name: "Superpowers",
                columns: table => new
                {
                    SuperpowerId = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Name = table.Column<string>(type: "nvarchar(max)", nullable: false),
                    Description = table.Column<string>(type: "nvarchar(max)", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Superpowers", x => x.SuperpowerId);
                });

            migrationBuilder.CreateTable(
                name: "SuperheroSuperpower",
                columns: table => new
                {
                    SuperheroesSuperheroId = table.Column<int>(type: "int", nullable: false),
                    SuperpowersSuperpowerId = table.Column<int>(type: "int", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_SuperheroSuperpower", x => new { x.SuperheroesSuperheroId, x.SuperpowersSuperpowerId });
                    table.ForeignKey(
                        name: "FK_SuperheroSuperpower_Superheros_SuperheroesSuperheroId",
                        column: x => x.SuperheroesSuperheroId,
                        principalTable: "Superheros",
                        principalColumn: "SuperheroId",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_SuperheroSuperpower_Superpowers_SuperpowersSuperpowerId",
                        column: x => x.SuperpowersSuperpowerId,
                        principalTable: "Superpowers",
                        principalColumn: "SuperpowerId",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.InsertData(
                table: "Superheros",
                columns: new[] { "SuperheroId", "DateOfBirth", "Name", "Nickname", "PhoneNumber" },
                values: new object[,]
                {
                    { 1, new DateTime(1980, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), "Superman", "Man of Steel", "123-456-7890" },
                    { 2, new DateTime(1985, 5, 10, 0, 0, 0, 0, DateTimeKind.Unspecified), "Batman", "Dark Knight", "987-654-3210" },
                    { 3, new DateTime(1990, 12, 25, 0, 0, 0, 0, DateTimeKind.Unspecified), "Wonder Woman", "Amazon Princess", "555-555-5555" },
                    { 4, new DateTime(1988, 6, 30, 0, 0, 0, 0, DateTimeKind.Unspecified), "The Flash", "Scarlet Speedster", "444-444-4444" },
                    { 5, new DateTime(1975, 3, 15, 0, 0, 0, 0, DateTimeKind.Unspecified), "Aquaman", "King of Atlantis", "111-111-1111" },
                    { 6, new DateTime(1982, 9, 5, 0, 0, 0, 0, DateTimeKind.Unspecified), "Green Lantern", "Emerald Knight", "222-222-2222" },
                    { 7, new DateTime(1920, 7, 4, 0, 0, 0, 0, DateTimeKind.Unspecified), "Captain America", "First Avenger", "777-777-7777" },
                    { 8, new DateTime(1970, 5, 29, 0, 0, 0, 0, DateTimeKind.Unspecified), "Iron Man", "Armored Avenger", "666-666-6666" },
                    { 9, new DateTime(1000, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), "Thor", "God of Thunder", "999-999-9999" },
                    { 10, new DateTime(1965, 6, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), "Hulk", "Incredible Hulk", "888-888-8888" },
                    { 11, new DateTime(1984, 11, 22, 0, 0, 0, 0, DateTimeKind.Unspecified), "Black Widow", "Natasha Romanoff", "123-123-1234" },
                    { 12, new DateTime(2001, 8, 10, 0, 0, 0, 0, DateTimeKind.Unspecified), "Spider-Man", "Friendly Neighborhood Spider-Man", "234-234-2345" },
                    { 13, new DateTime(1987, 5, 7, 0, 0, 0, 0, DateTimeKind.Unspecified), "Sean O'Donnell-Daudlin", "Sean", "313-123-1234" },
                    { 14, new DateTime(1992, 9, 23, 0, 0, 0, 0, DateTimeKind.Unspecified), "Emma Beasley", "Dr. Emma", "248-234-2345" }
                });

            migrationBuilder.InsertData(
                table: "Superpowers",
                columns: new[] { "SuperpowerId", "Description", "Name" },
                values: new object[,]
                {
                    { 1, "Ability to fly", "Flight" },
                    { 2, "Enhanced strength", "Super strength" },
                    { 3, "Ability to read minds", "Telepathy" },
                    { 4, "Ability to move objects with the mind", "Telekinesis" },
                    { 5, "Ability to become invisible", "Invisibility" },
                    { 6, "Ability to project energy blasts", "Energy projection" },
                    { 7, "Ability to manipulate time", "Time manipulation" },
                    { 8, "Ability to heal rapidly", "Healing factor" },
                    { 9, "Ability to run very quickly", "Running" },
                    { 10, "Ability to heal patients rapidly", "Medical Expertise" },
                    { 11, "Ability to code rapidly", "Software Developing" }
                });

            migrationBuilder.InsertData(
                table: "SuperheroSuperpower",
                columns: new[] { "SuperheroesSuperheroId", "SuperpowersSuperpowerId" },
                values: new object[,]
                {
                    { 1, 1 },
                    { 1, 2 },
                    { 1, 3 },
                    { 1, 4 },
                    { 2, 2 },
                    { 2, 5 },
                    { 3, 2 },
                    { 3, 3 },
                    { 3, 6 },
                    { 4, 1 },
                    { 4, 4 },
                    { 5, 1 },
                    { 5, 6 },
                    { 5, 7 },
                    { 6, 1 },
                    { 6, 6 },
                    { 7, 2 },
                    { 7, 7 },
                    { 8, 6 },
                    { 8, 8 },
                    { 9, 2 },
                    { 9, 6 },
                    { 9, 7 },
                    { 10, 2 },
                    { 10, 4 },
                    { 10, 6 },
                    { 11, 5 },
                    { 12, 1 },
                    { 12, 2 },
                    { 12, 4 },
                    { 13, 9 },
                    { 13, 11 },
                    { 14, 9 },
                    { 14, 10 }
                });

            migrationBuilder.CreateIndex(
                name: "IX_SuperheroSuperpower_SuperpowersSuperpowerId",
                table: "SuperheroSuperpower",
                column: "SuperpowersSuperpowerId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "SuperheroSuperpower");

            migrationBuilder.DropTable(
                name: "Superheros");

            migrationBuilder.DropTable(
                name: "Superpowers");
        }
    }
}
