﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;


namespace Superhero.Api.Models
{
    public class SuperheroContext : DbContext
    {

        public SuperheroContext(DbContextOptions<SuperheroContext> options) : base(options)
        {
            //Database.SetInitializer(new DropCreateDatabaseAlways<SuperheroContext>());
        }

        public DbSet<Superhero> Superheros { get; set; }
        public DbSet<Superpower> Superpowers { get; set; }

        //public DbSet<SuperheroSuperpower> SuperheroSuperpower { get; set; }

        //public virtual DbSet<GetMoviesByCategory> GetMoviesByCategory { get; set; }
        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            optionsBuilder.UseSqlServer("Data Source=(localdb)\\MSSQLLocalDB;Initial Catalog=SuperheroDB;Integrated Security=True;Connect Timeout=30;Encrypt=False;TrustServerCertificate=False;ApplicationIntent=ReadWrite;MultiSubnetFailover=False").EnableSensitiveDataLogging();
        }

        //        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        //#warning To protect potentially sensitive information in your connection string, you should move it out of source code. You can avoid scaffolding the connection string by using the Name= syntax to read it from configuration - see https://go.microsoft.com/fwlink/?linkid=2131148. For more guidance on storing connection strings, see http://go.microsoft.com/fwlink/?LinkId=723263.
        //        => optionsBuilder.UseSqlServer("Data Source=(localdb)\\MSSQLLocalDB;Initial Catalog=Northwind;Integrated Security=True;Connect Timeout=30;Encrypt=False;TrustServerCertificate=False;ApplicationIntent=ReadWrite;MultiSubnetFailover=False");

        protected override void OnModelCreating(
        ModelBuilder modelBuilder)
        {
            //        modelBuilder.Entity<Superhero>()
            //.HasMany(s => s.Superpowers)
            //.WithMany(sp => sp.Superheroes)
            //.UsingEntity<SuperheroSuperpower>(
            //    j => j
            //        .HasOne(ssp => ssp.SuperpowerId)
            //        .WithMany()
            //        .HasForeignKey(ssp => ssp.SuperpowerId)
            //        .OnDelete(DeleteBehavior.Restrict),
            //    j => j
            //        .HasOne(ssp => ssp.Superhero)
            //        .WithMany()
            //        .HasForeignKey(ssp => ssp.SuperheroId)
            //        .OnDelete(DeleteBehavior.Restrict),
            //    j =>
            //    {
            //        j.HasKey(ssp => new { ssp.SuperheroId, ssp.SuperpowerId });
            //    });
            /////////
            ///
            /// 
            /// 

            modelBuilder.Entity<Superhero>()
        .HasMany(s => s.Superpowers);
            //.WithMany(sp => sp.Superheroes)
            //.UsingEntity<SuperheroSuperpower>(
            //    j => j.HasData(
            //        new SuperheroSuperpower { SuperheroId = 1, SuperpowerId = 1 },
            //        new SuperheroSuperpower { SuperheroId = 1, SuperpowerId = 2 },
            //        new SuperheroSuperpower { SuperheroId = 1, SuperpowerId = 3 },
            //        new SuperheroSuperpower { SuperheroId = 1, SuperpowerId = 4 },
            //        new SuperheroSuperpower { SuperheroId = 2, SuperpowerId = 2 },
            //        new SuperheroSuperpower { SuperheroId = 2, SuperpowerId = 5 },
            //        new SuperheroSuperpower { SuperheroId = 2, SuperpowerId = 6 },
            //        new SuperheroSuperpower { SuperheroId = 2, SuperpowerId = 7 }
            //        // Add more relationships as needed
            //    ));

            // Seed Superheroes



            // Seed Superheroes
            //modelBuilder.Entity<Superhero>().HasData(
            //    new Superhero
            //    {
            //        SuperheroId = 1,
            //        Name = "Iron Man",
            //        Nickname = "Tony Stark",
            //        PhoneNumber = "1234567890",
            //        DateOfBirth = new DateTime(1970, 5, 29)

            //    },
            //    new Superhero
            //    {
            //        SuperheroId = 2,
            //        Name = "Captain America",
            //        Nickname = "Steve Rogers",
            //        PhoneNumber = "2345678901",
            //        DateOfBirth = new DateTime(1918, 7, 4)

            //    }
            //);

            // Seed Superpowers
            modelBuilder.Entity<Superpower>().HasData(
                new Superpower { SuperpowerId = 1, Name = "Flight", Description = "Ability to fly" },
                new Superpower { SuperpowerId = 2, Name = "Super strength", Description = "Enhanced strength" },
                new Superpower { SuperpowerId = 3, Name = "Telepathy", Description = "Ability to read minds" },
                new Superpower { SuperpowerId = 4, Name = "Telekinesis", Description = "Ability to move objects with the mind" },
                new Superpower { SuperpowerId = 5, Name = "Invisibility", Description = "Ability to become invisible" },
                new Superpower { SuperpowerId = 6, Name = "Energy projection", Description = "Ability to project energy blasts" },
                new Superpower { SuperpowerId = 7, Name = "Time manipulation", Description = "Ability to manipulate time" },
                new Superpower { SuperpowerId = 8, Name = "Healing factor", Description = "Ability to heal rapidly" },
                new Superpower { SuperpowerId = 9, Name = "Running", Description = "Ability to run very quickly" },
                new Superpower { SuperpowerId = 10, Name = "Medical Expertise", Description = "Ability to heal patients rapidly" },
                new Superpower { SuperpowerId = 11, Name = "Software Developing", Description = "Ability to code rapidly" }
            );



            /////
            ///
            /// 
            //// Seed Superheroes
            var superhero1 = new Superhero { SuperheroId = 1, Name = "Superman", Nickname = "Man of Steel", PhoneNumber = "123-456-7890", DateOfBirth = new DateTime(1980, 1, 1) };
            var superhero2 = new Superhero { SuperheroId = 2, Name = "Batman", Nickname = "Dark Knight", PhoneNumber = "987-654-3210", DateOfBirth = new DateTime(1985, 5, 10) };
            var superhero3 = new Superhero { SuperheroId = 3, Name = "Wonder Woman", Nickname = "Amazon Princess", PhoneNumber = "555-555-5555", DateOfBirth = new DateTime(1990, 12, 25) };
            var superhero4 = new Superhero { SuperheroId = 4, Name = "The Flash", Nickname = "Scarlet Speedster", PhoneNumber = "444-444-4444", DateOfBirth = new DateTime(1988, 6, 30) };
            var superhero5 = new Superhero { SuperheroId = 5, Name = "Aquaman", Nickname = "King of Atlantis", PhoneNumber = "111-111-1111", DateOfBirth = new DateTime(1975, 3, 15) };
            var superhero6 = new Superhero { SuperheroId = 6, Name = "Green Lantern", Nickname = "Emerald Knight", PhoneNumber = "222-222-2222", DateOfBirth = new DateTime(1982, 9, 5) };
            var superhero7 = new Superhero { SuperheroId = 7, Name = "Captain America", Nickname = "First Avenger", PhoneNumber = "777-777-7777", DateOfBirth = new DateTime(1920, 7, 4) };
            var superhero8 = new Superhero { SuperheroId = 8, Name = "Iron Man", Nickname = "Armored Avenger", PhoneNumber = "666-666-6666", DateOfBirth = new DateTime(1970, 5, 29) };
            var superhero9 = new Superhero { SuperheroId = 9, Name = "Thor", Nickname = "God of Thunder", PhoneNumber = "999-999-9999", DateOfBirth = new DateTime(1000, 1, 1) };
            var superhero10 = new Superhero { SuperheroId = 10, Name = "Hulk", Nickname = "Incredible Hulk", PhoneNumber = "888-888-8888", DateOfBirth = new DateTime(1965, 6, 1) };
            var superhero11 = new Superhero { SuperheroId = 11, Name = "Black Widow", Nickname = "Natasha Romanoff", PhoneNumber = "123-123-1234", DateOfBirth = new DateTime(1984, 11, 22) };
            var superhero12 = new Superhero { SuperheroId = 12, Name = "Spider-Man", Nickname = "Friendly Neighborhood Spider-Man", PhoneNumber = "234-234-2345", DateOfBirth = new DateTime(2001, 8, 10) };
            var superhero13 = new Superhero { SuperheroId = 13, Name = "Sean O'Donnell-Daudlin", Nickname = "Sean", PhoneNumber = "313-123-1234", DateOfBirth = new DateTime(1987, 05, 07) };
            var superhero14 = new Superhero { SuperheroId = 14, Name = "Emma Beasley", Nickname = "Dr. Emma", PhoneNumber = "248-234-2345", DateOfBirth = new DateTime(1992, 9, 23) };

            modelBuilder.Entity<Superhero>().HasData(superhero1, superhero2, superhero3, superhero4, superhero5, superhero6, superhero7, superhero8, superhero9, superhero10, superhero11, superhero12,superhero13, superhero14);

            ////////////////
            //var actorMovieBuilder = modelBuilder.Entity("SuperheroSuperpower").HasData(
            //new { SuperheroesSuperheroId = 1, SuperpowersSuperpowerId = 1 },
            //new { SuperheroesSuperheroId = 1, SuperpowersSuperpowerId = 4 },
            //new { SuperheroesSuperheroId = 2, SuperpowersSuperpowerId = 2 }
            //);

            modelBuilder.Entity("SuperheroSuperpower").HasData(
    // Superman
    new { SuperheroesSuperheroId = 1, SuperpowersSuperpowerId = 1 },
    new { SuperheroesSuperheroId = 1, SuperpowersSuperpowerId = 2 },
    new { SuperheroesSuperheroId = 1, SuperpowersSuperpowerId = 3 },
    new { SuperheroesSuperheroId = 1, SuperpowersSuperpowerId = 4 },
    // Batman
    new { SuperheroesSuperheroId = 2, SuperpowersSuperpowerId = 2 },
    new { SuperheroesSuperheroId = 2, SuperpowersSuperpowerId = 5 },
    // Wonder Woman
    new { SuperheroesSuperheroId = 3, SuperpowersSuperpowerId = 2 },
    new { SuperheroesSuperheroId = 3, SuperpowersSuperpowerId = 3 },
    new { SuperheroesSuperheroId = 3, SuperpowersSuperpowerId = 6 },
    // The Flash
    new { SuperheroesSuperheroId = 4, SuperpowersSuperpowerId = 1 },
    new { SuperheroesSuperheroId = 4, SuperpowersSuperpowerId = 4 },
    // Aquaman
    new { SuperheroesSuperheroId = 5, SuperpowersSuperpowerId = 1 },
    new { SuperheroesSuperheroId = 5, SuperpowersSuperpowerId = 6 },
    new { SuperheroesSuperheroId = 5, SuperpowersSuperpowerId = 7 },
    // Green Lantern
    new { SuperheroesSuperheroId = 6, SuperpowersSuperpowerId = 1 },
    new { SuperheroesSuperheroId = 6, SuperpowersSuperpowerId = 6 },
    // Captain America
    new { SuperheroesSuperheroId = 7, SuperpowersSuperpowerId = 2 },
    new { SuperheroesSuperheroId = 7, SuperpowersSuperpowerId = 7 },
    // Iron Man
    new { SuperheroesSuperheroId = 8, SuperpowersSuperpowerId = 6 },
    new { SuperheroesSuperheroId = 8, SuperpowersSuperpowerId = 8 },
    // Thor
    new { SuperheroesSuperheroId = 9, SuperpowersSuperpowerId = 2 },
    new { SuperheroesSuperheroId = 9, SuperpowersSuperpowerId = 6 },
    new { SuperheroesSuperheroId = 9, SuperpowersSuperpowerId = 7 },
    // Hulk
    new { SuperheroesSuperheroId = 10, SuperpowersSuperpowerId = 2 },
    new { SuperheroesSuperheroId = 10, SuperpowersSuperpowerId = 4 },
    new { SuperheroesSuperheroId = 10, SuperpowersSuperpowerId = 6 },
    // Black Widow
    new { SuperheroesSuperheroId = 11, SuperpowersSuperpowerId = 5 },
    // Spider-Man
    new { SuperheroesSuperheroId = 12, SuperpowersSuperpowerId = 1 },
    new { SuperheroesSuperheroId = 12, SuperpowersSuperpowerId = 2 },
    new { SuperheroesSuperheroId = 12, SuperpowersSuperpowerId = 4 },

    // Sean
    new { SuperheroesSuperheroId = 13, SuperpowersSuperpowerId = 9 },
    new { SuperheroesSuperheroId = 13, SuperpowersSuperpowerId = 11 },
    // Emma
    new { SuperheroesSuperheroId = 14, SuperpowersSuperpowerId = 9 },
    new { SuperheroesSuperheroId = 14, SuperpowersSuperpowerId = 10 }
);


            //modelBuilder.Entity<Superhero>()
            //    .HasKey(x => x.SuperheroId);

            //    modelBuilder.Entity<Superhero>()
            //.HasMany(s => s.Superpowers)
            //.WithMany(s => s.Superheroes)
            //.UsingEntity<Dictionary<string, object>>(
            //    "SuperheroSuperpower",
            //    j => j
            //        .HasOne<Superpower>()
            //        .WithMany()
            //        .HasForeignKey("SuperpowerId"),
            //    j => j
            //        .HasOne<Superhero>()
            //        .WithMany()
            //        .HasForeignKey("SuperheroId")
            //);
            //.HasMany(category => category.Movies)
            //.WithOne(movie => movie.Category);
            ////.HasForeignKey(movie => .AuthorID);

            //modelBuilder.Entity<GetMoviesByCategory>(entity =>
            //{
            //    entity
            //    .HasNoKey();
            //});

            //            modelBuilder.Entity<Superpower>().HasData(
            //    new Superpower { SuperpowerId = 1, Name = "Flight", Description = "Ability to fly" },
            //    new Superpower { SuperpowerId = 2, Name = "Super strength", Description = "Enhanced strength" },
            //    new Superpower { SuperpowerId = 3, Name = "Telepathy", Description = "Ability to read minds" },
            //    new Superpower { SuperpowerId = 4, Name = "Telekinesis", Description = "Ability to move objects with the mind" },
            //    new Superpower { SuperpowerId = 5, Name = "Invisibility", Description = "Ability to become invisible" },
            //    new Superpower { SuperpowerId = 6, Name = "Energy projection", Description = "Ability to project energy blasts" },
            //    new Superpower { SuperpowerId = 7, Name = "Time manipulation", Description = "Ability to manipulate time" },
            //    new Superpower { SuperpowerId = 8, Name = "Healing factor", Description = "Ability to heal rapidly" },
            //    new Superpower { SuperpowerId = 9, Name = "Super speed", Description = "Enhanced speed" },
            //    new Superpower { SuperpowerId = 10, Name = "Reality manipulation", Description = "Ability to manipulate reality" },
            //    // Add more superpowers as needed
            //    new Superpower { SuperpowerId = 11, Name = "Regeneration", Description = "Ability to regenerate lost tissues" },
            //    new Superpower { SuperpowerId = 12, Name = "Weather manipulation", Description = "Ability to control weather elements" }
            //);

            //// Seed Superheroes
            //modelBuilder.Entity<Superhero>().HasData(
            //    new Superhero
            //    {
            //        SuperheroId = 1,
            //        Name = "Iron Man",
            //        Nickname = "Tony Stark",
            //        PhoneNumber = "1234567890",
            //        DateOfBirth = new DateTime(1970, 5, 29),
            //        Superpowers = new List<Superpower>
            //    {
            //new  { SuperpowerId = 1 },
            //new { SuperpowerId = 2 },
            //new { SuperpowerId = 3 },
            //new { SuperpowerId = 4 },
            //new { SuperpowerId = 5 },
            //new { SuperpowerId = 6 },
            //new { SuperpowerId = 7 },
            //new { SuperpowerId = 8 },
            //new { SuperpowerId = 9 },
            //new { SuperpowerId = 10 }
            //    }
            //    });
            //,
            //    new Superhero
            //    {
            //        SuperheroId = 2,
            //        Name = "Captain America",
            //        Nickname = "Steve Rogers",
            //        PhoneNumber = "2345678901",
            //        DateOfBirth = new DateTime(1918, 7, 4),
            //        Superpowers = new List<Superpower>
            //    {
            //new Superpower { SuperpowerId = 2 },
            //new Superpower { SuperpowerId = 5 },
            //new Superpower { SuperpowerId = 8 }
            //    }
            //    },
            //    new Superhero
            //    {
            //        SuperheroId = 3,
            //        Name = "Thor",
            //        Nickname = "Odinson",
            //        PhoneNumber = "3456789012",
            //        DateOfBirth = new DateTime(1000, 1, 1),
            //        Superpowers = new List<Superpower>
            //    {
            //new Superpower { SuperpowerId = 1 },
            //new Superpower { SuperpowerId = 2 },
            //new Superpower { SuperpowerId = 4 },
            //new Superpower { SuperpowerId = 9 }
            //    }
            //    });




            //        modelBuilder.Entity<Superhero>().HasData(
            //    new Superhero
            //    {
            //        SuperheroId = 1,
            //        Name = "Iron Man",
            //        Nickname = "Tony Stark",
            //        Superpower = "Genius-level intellect, powered exoskeleton",
            //        PhoneNumber = "+1-555-1234",
            //        DateOfBirth = new DateTime(1970, 5, 29)
            //    },
            //    new Superhero
            //    {
            //        SuperheroId = 2,
            //        Name = "Captain America",
            //        Nickname = "Steve Rogers",
            //        Superpower = "Enhanced strength, agility, endurance, and shield",
            //        PhoneNumber = "+1-555-5678",
            //        DateOfBirth = new DateTime(1920, 7, 4)
            //    },
            //    new Superhero
            //    {
            //        SuperheroId = 3,
            //        Name = "Black Widow",
            //        Nickname = "Natasha Romanoff",
            //        Superpower = "Expert spy, martial artist, and marksman",
            //        PhoneNumber = "+1-555-9876",
            //        DateOfBirth = new DateTime(1984, 11, 22)
            //    }
            ////new Superhero { SuperheroId = 1, Breed = "Maine Coon", Name = "Ziggy", Nickname = "Emma", Species = "Cat", CheckInDate = new DateTime(2023, 4, 5), CheckOutDate = new DateTime(2023, 4, 9), },
            ////new Superhero { SuperheroId = 2, Breed = "Tabby", Name = "Orangie", Nickname = "Sean", Species = "Cat", CheckInDate = new DateTime(2023, 3, 5), CheckOutDate = new DateTime(2023, 4, 9), }


            //);


            //// Seed Superheroes
            //var superhero1 = new Superhero { Name = "Superman", Nickname = "Man of Steel", PhoneNumber = "123-456-7890", DateOfBirth = new DateTime(1980, 1, 1) };
            //var superhero2 = new Superhero { Name = "Batman", Nickname = "Dark Knight", PhoneNumber = "987-654-3210", DateOfBirth = new DateTime(1985, 5, 10) };
            //var superhero3 = new Superhero { Name = "Wonder Woman", Nickname = "Amazon Princess", PhoneNumber = "555-555-5555", DateOfBirth = new DateTime(1990, 12, 25) };
            //var superhero4 = new Superhero { Name = "The Flash", Nickname = "Scarlet Speedster", PhoneNumber = "444-444-4444", DateOfBirth = new DateTime(1988, 6, 30) };
            //var superhero5 = new Superhero { Name = "Aquaman", Nickname = "King of Atlantis", PhoneNumber = "111-111-1111", DateOfBirth = new DateTime(1975, 3, 15) };
            //var superhero6 = new Superhero { Name = "Green Lantern", Nickname = "Emerald Knight", PhoneNumber = "222-222-2222", DateOfBirth = new DateTime(1982, 9, 5) };
            //var superhero7 = new Superhero { Name = "Captain America", Nickname = "First Avenger", PhoneNumber = "777-777-7777", DateOfBirth = new DateTime(1920, 7, 4) };
            //var superhero8 = new Superhero { Name = "Iron Man", Nickname = "Armored Avenger", PhoneNumber = "666-666-6666", DateOfBirth = new DateTime(1970, 5, 29) };
            //var superhero9 = new Superhero { Name = "Thor", Nickname = "God of Thunder", PhoneNumber = "999-999-9999", DateOfBirth = new DateTime(1000, 1, 1) };
            //var superhero10 = new Superhero { Name = "Hulk", Nickname = "Incredible Hulk", PhoneNumber = "888-888-8888", DateOfBirth = new DateTime(1965, 6, 1) };
            //var superhero11 = new Superhero { Name = "Black Widow", Nickname = "Natasha Romanoff", PhoneNumber = "123-123-1234", DateOfBirth = new DateTime(1984, 11, 22) };
            //var superhero12 = new Superhero { Name = "Spider-Man", Nickname = "Friendly Neighborhood Spider-Man", PhoneNumber = "234-234-2345", DateOfBirth = new DateTime(2001, 8, 10) };

            //modelBuilder.Entity<Superhero>().HasData(superhero1, superhero2, superhero3, superhero4, superhero5, superhero6, superhero7, superhero8, superhero9, superhero10, superhero11, superhero12);


            //            modelBuilder.Entity<Superpower>().HasData(


            //new { SuperpowerId = 1, Name = "Flight", Description = "Ability to fly", SuperheroId = 1 },
            //new { SuperpowerId = 2, Name = "Super strength", Description = "Enhanced strength", SuperheroId = 1 },
            ////new { SuperpowerId = 2, Name = "Super strength", Description = "Enhanced strength", SuperheroId = 2 },
            //new  { SuperpowerId = 3, Name = "Telepathy", Description = "Ability to read minds", SuperheroId = 2 },
            //new  { SuperpowerId = 4, Name = "Telekinesis", Description = "Ability to move objects with the mind", SuperheroId = 3 }

            //            //// Associate Superpowers with Superheroes
            //            //superhero1.Superpowers.Add(superpower1);
            //            //superhero1.Superpowers.Add(superpower2);
            //            //superhero2.Superpowers.Add(superpower2);
            //            //superhero2.Superpowers.Add(superpower3);
            //            //superhero3.Superpowers.Add(superpower4);


            //            );

            //modelBuilder.Entity<Superpower>().HasData(
            //    new Superpower { SuperpowerId = 1, Name = "Flight", Description = "Ability to fly" },
            //    new Superpower { SuperpowerId = 2, Name = "Super strength", Description = "Enhanced strength" },
            //    new Superpower { SuperpowerId = 3, Name = "Telepathy", Description = "Ability to read minds" },
            //    new Superpower { SuperpowerId = 4, Name = "Telekinesis", Description = "Ability to move objects with the mind" },
            //    // Add more superpowers as needed
            //    new Superpower { SuperpowerId = 5, Name = "Invisibility", Description = "Ability to become invisible" },
            //    new Superpower { SuperpowerId = 6, Name = "Energy projection", Description = "Ability to project energy blasts" },
            //    new Superpower { SuperpowerId = 7, Name = "Time manipulation", Description = "Ability to manipulate time" },
            //    new Superpower { SuperpowerId = 8, Name = "Healing factor", Description = "Ability to heal rapidly" },
            //    new Superpower { SuperpowerId = 9, Name = "Super speed", Description = "Enhanced speed" },
            //    new Superpower { SuperpowerId = 10, Name = "Reality manipulation", Description = "Ability to manipulate reality" }
            //);

            // Seed Superheroes
            //modelBuilder.Entity<Superhero>().HasData(
            //    new Superhero
            //    {
            //        SuperheroId = 1,
            //        Name = "Iron Man",
            //        Nickname = "Tony Stark",
            //        PhoneNumber = "1234567890",
            //        DateOfBirth = new DateTime(1970, 5, 29),
            //        Superpowers = new List<Superpower>()
            //    {
            //    new Superpower{ Name = "Flight", Description = "Ability to fly" },
            //    }
            //    }
            //    },
            //    new Superhero
            //    {
            //        SuperheroId = 2,
            //        Name = "Captain America",
            //        Nickname = "Steve Rogers",
            //        PhoneNumber = "2345678901",
            //        DateOfBirth = new DateTime(1918, 7, 4),
            //        Superpowers = new List<Superpower>()
            //    {
            //new Superpower { SuperpowerId = 2 },
            //new Superpower { SuperpowerId = 5 },
            //new Superpower { SuperpowerId = 8 }
            //    }
            //    },
            //    new Superhero
            //    {
            //        SuperheroId = 3,
            //        Name = "Thor",
            //        Nickname = "Odinson",
            //        PhoneNumber = "3456789012",
            //        DateOfBirth = new DateTime(1000, 1, 1),
            //        Superpowers = new List<Superpower>()
            //    {
            //new Superpower { SuperpowerId = 1 },
            //new Superpower { SuperpowerId = 2 },
            //new Superpower { SuperpowerId = 4 },
            //new Superpower { SuperpowerId = 9 }
            //    }
            //    }
            //);

        }
    }
}





