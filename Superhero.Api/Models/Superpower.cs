﻿namespace Superhero.Api.Models
{
    public class Superpower
    {
        public int SuperpowerId { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public virtual List<Superhero> Superheroes { get; set; } = new(); // Many-to-many relationship with Superheroes
    }
}
