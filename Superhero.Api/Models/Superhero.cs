﻿namespace Superhero.Api.Models
{
    public class Superhero
    {
        public int SuperheroId { get; set; }
        public string Name { get; set; }
        public string Nickname { get; set; }
        //public string Superpower { get; set; }
        public string PhoneNumber { get; set; }
        public DateTime DateOfBirth { get; set; }
        public virtual List<Superpower> Superpowers { get; set; } = new();// Many-to-many relationship with Superpowers

    }
}

