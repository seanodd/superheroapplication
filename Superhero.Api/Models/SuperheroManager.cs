﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;

namespace Superhero.Api.Models
{
    public class SuperheroManager
    {
        private readonly SuperheroContext superheroContext;
        public SuperheroManager(SuperheroContext superheroContext)
        {
            this.superheroContext = superheroContext;
        }

        public async Task<IEnumerable<Superhero>> GetSuperheros()
        {
            return await superheroContext.Superheros.ToListAsync();

        }



        //////////////////
        //////This returns one, needs to be updated to return list
        public async Task<Superhero> GetByNickname(string nickname)
        {
            return await superheroContext.Superheros
                .FirstOrDefaultAsync(a => a.Nickname == nickname);
        }

        //[HttpGet]
        //public IEnumerable<Superhero> GetAllSuperheros()
        //{
        //List<Superhero> superheros = null;
        //using (var context = new SuperheroShelterContext())
        //{
        //superheros = context.Superheros.ToList();
        //}
        //return superheros;
        //}
        //[HttpGet("{id}")]
        //public Superhero GetSuperheroById(string id)
        //{
        //Superhero superhero = null;
        //using (var context = new SuperheroShelterContext())
        //{
        //superhero = context.Superheros
        //.Where(c => c.SuperheroID == id)
        //.SingleOrDefault();
        //}
        //return superhero;
        //}
    }
}
